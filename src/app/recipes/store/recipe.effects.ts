import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, switchMap, withLatestFrom} from 'rxjs/Operators';
import {HttpClient} from '@angular/common/http';

import * as RecipesAction from './recipe.action';
import {Recipe} from '../recipe.model';
import * as fromApp from '../../store/app.reducer';
import {Store} from '@ngrx/store';

@Injectable()
export class RecipeEffects {

  recipesUrl = 'https://recipe-book-7ef3c-default-rtdb.europe-west1.firebasedatabase.app/recipes.json';

  @Effect()
  fetchRecipes = this.actions.pipe(
    ofType(RecipesAction.FETCH_RECIPES),
    switchMap(() => {
      return this.http.get<Recipe[]>(this.recipesUrl);
    }),
    map(recipes => {
      return recipes.map(recipe => {
        return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
      });
    }),
    map(recipes => {
      return new RecipesAction.SetRecipes(recipes);
    })
  );

  @Effect({dispatch: false})
  storeRecipes = this.actions.pipe(
    ofType(RecipesAction.STORE_RECIPES),
    withLatestFrom(this.store.select('recipes')),
    switchMap(([actionData, recipesState]) => {
      return this.http.put(this.recipesUrl, recipesState.recipes);
    })
  );

  constructor(private actions: Actions,
              private http: HttpClient,
              private store: Store<fromApp.AppState>) {
  }
}
